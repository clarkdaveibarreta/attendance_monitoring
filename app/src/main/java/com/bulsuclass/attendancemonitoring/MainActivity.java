package com.bulsuclass.attendancemonitoring;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.bulsuclass.attendancemonitoring.pages.AttendanceActivity;
import com.bulsuclass.attendancemonitoring.pages.ConnectActivity;
import com.bulsuclass.attendancemonitoring.pages.HomeActivity;
import com.bulsuclass.attendancemonitoring.pages.SubjectActivity;

public class MainActivity extends AppCompatActivity {

    private Button classesButton, attendanceButton, connectButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

//        startActivity(new Intent(MainActivity.this, HomeActivity.class));
//        setContentView(R.layout.activity_first);


        classesButton = findViewById(R.id.classes_button);
        attendanceButton = findViewById(R.id.attendance_button);
        connectButton = findViewById(R.id.connect_button);

        classesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openClassesActivity();
            }
        });

        attendanceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAttendanceActivity();
            }
        });

        connectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openConnectActivity();
            }
        });
    }

    private void openClassesActivity() {
        //TODO: open CLassesActivity here ...
        startActivity(new Intent(MainActivity.this, SubjectActivity.class));
    }

    private void openAttendanceActivity() {
        //TODO: open JoinActivity here ...
        startActivity(new Intent(MainActivity.this, AttendanceActivity.class));
    }

    private void openConnectActivity() {
        //TODO: open ConnectActivity here ...
        startActivity(new Intent(MainActivity.this, ConnectActivity.class));
    }

}