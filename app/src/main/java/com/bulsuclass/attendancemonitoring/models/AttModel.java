package com.bulsuclass.attendancemonitoring.models;

public class AttModel {

    String id;
    String studentId;
    String subjectId;
    String date;
    String attended;
    String ip;

    public AttModel(String id, String studentId, String subjectId, String date, String attended, String ip) {
        this.id = id;
        this.studentId = studentId;
        this.subjectId = subjectId;
        this.date = date;
        this.attended = attended;
        this.ip = ip;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAttended() {
        return attended;
    }

    public void setAttended(String attended) {
        this.attended = attended;
    }

    public String getIp() {
        if(this.ip == null) {
            this.ip = "N/A";
        }
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
