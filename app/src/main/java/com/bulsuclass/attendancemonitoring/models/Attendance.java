package com.bulsuclass.attendancemonitoring.models;

public class Attendance {

    private String attendanceId;
    private Student student;
    private Subject subject;
    private String date;
    private String attended;
    private String ip;

    public Attendance(String attendanceId, Student student, Subject subject, String date, String attended) {
        this.attendanceId = attendanceId;
        this.student = student;
        this.subject = subject;
        this.date = date;
        this.attended = attended;
    }

    public Attendance(String attendanceId, Student student, Subject subject, String date, String attended, String ip) {
        this.attendanceId = attendanceId;
        this.student = student;
        this.subject = subject;
        this.date = date;
        this.attended = attended;
        this.ip = ip;
    }

    public String getAttendanceId() {
        return attendanceId;
    }

    public void setAttendanceId(String attendanceId) {
        this.attendanceId = attendanceId;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAttended() {
        return attended;
    }

    public void setAttended(String attended) {
        this.attended = attended;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
