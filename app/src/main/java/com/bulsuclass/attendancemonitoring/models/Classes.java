package com.bulsuclass.attendancemonitoring.models;

public class Classes {

    private String classId;
    private String classSubjectId;
    private String classStudentId;

    public Classes(String classId, String classSubjectId, String classStudentId) {
        this.classId = classId;
        this.classSubjectId = classSubjectId;
        this.classStudentId = classStudentId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassSubjectId() {
        return classSubjectId;
    }

    public void setClassSubjectId(String classSubjectId) {
        this.classSubjectId = classSubjectId;
    }

    public String getClassStudentId() {
        return classStudentId;
    }

    public void setClassStudentId(String classStudentId) {
        this.classStudentId = classStudentId;
    }
}
