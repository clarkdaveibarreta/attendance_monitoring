package com.bulsuclass.attendancemonitoring.helpers;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.bulsuclass.attendancemonitoring.R;
import com.bulsuclass.attendancemonitoring.models.Student;

public class AddStudentDialog extends AppCompatDialogFragment {

    private EditText editTextStudentId, editTextStudentFirstName, editTextStudentLastName, editTextStudentmi;

    private AddStudentDialogListener listener;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_add_student, null);

        builder.setView(view)
                .setTitle("Add Student")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Cancel the adding process
                    }
                })
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //TODO: add student process
                        String studentId = editTextStudentId.getText().toString();
                        String firstName = editTextStudentFirstName.getText().toString();
                        String lastName = editTextStudentLastName.getText().toString();
                        String mi = editTextStudentmi.getText().toString();

                        listener.AddStudent(new Student(studentId, firstName, lastName, mi));

                    }
                });

        editTextStudentId = view.findViewById(R.id.edit_text_student_id);
        editTextStudentFirstName = view.findViewById(R.id.edit_text_student_fn);
        editTextStudentLastName = view.findViewById(R.id.edit_text_student_ln);
        editTextStudentmi = view.findViewById(R.id.edit_text_student_mi);

        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            listener = (AddStudentDialogListener) context;
        }
        catch(ClassCastException e) {
            throw new ClassCastException(e + " must implement AddStudent method");
        }

    }

    public interface AddStudentDialogListener {
        void AddStudent(Student student);
    }
}
