package com.bulsuclass.attendancemonitoring.helpers;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.bulsuclass.attendancemonitoring.R;
import com.bulsuclass.attendancemonitoring.models.Subject;
import com.bulsuclass.attendancemonitoring.pages.SubjectActivity;

public class AddSubjectDialog extends AppCompatDialogFragment {

    private EditText editTextSubjectName;
    private EditText editTextSubjectTime;
    private AddSubjectDialogListener listener;

    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_add_subject, null);

        builder.setView(view)
                .setTitle("Add Subject")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Cancel the modal
                    }
                })
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //TODO: Store subject in the database
                        String subjectName = editTextSubjectName.getText().toString();
                        String subjectTime = editTextSubjectTime.getText().toString();
                        //if(subjectName.equals(false)) {}
                        Subject subject = new Subject(subjectName, subjectTime);

                        listener.saveSubject(subject);
                    }
                });

        editTextSubjectName = view.findViewById(R.id.edit_text_subject_name);
        editTextSubjectTime = view.findViewById(R.id.edit_text_subject_time);

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (AddSubjectDialogListener) context;
        }
        catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement AddSubjectDialogListener");
        }
    }

    public interface AddSubjectDialogListener {
        void saveSubject(Subject subject);
    }
}
