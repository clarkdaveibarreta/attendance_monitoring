package com.bulsuclass.attendancemonitoring.helpers;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.bulsuclass.attendancemonitoring.R;
import com.bulsuclass.attendancemonitoring.models.Subject;

import org.w3c.dom.Text;

public class UpdateSubjectDialog extends AppCompatDialogFragment {

    EditText editTextSubjectName, editTextSubjectTime;

    Subject subject;
    public UpdateSubjectDialogListener listener;

    public UpdateSubjectDialog(Subject subject) {
        super();
        this.subject = subject;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_add_subject, null);

        builder.setView(view)
                .setTitle("Update Subject")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // cancel the process
                    }
                })
                .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //TODO: update student process here
                        String subjectName = editTextSubjectName.getText().toString();
                        String subjectTime = editTextSubjectTime.getText().toString();

                        subject.setName(subjectName);
                        subject.setTime(subjectTime);

                        listener.updateSubject(subject);
                    }
                });

        editTextSubjectName = view.findViewById(R.id.edit_text_subject_name);
        editTextSubjectTime = view.findViewById(R.id.edit_text_subject_time);

        editTextSubjectName.setText(subject.getName());
        editTextSubjectTime.setText(subject.getTime());

        return builder.create();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (UpdateSubjectDialogListener) context;
        }
        catch(ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement UpdateSubjectDialogListener");
        }
    }

    public interface UpdateSubjectDialogListener {
        void updateSubject(Subject subject);
    }
}
