package com.bulsuclass.attendancemonitoring.controllers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.bulsuclass.attendancemonitoring.models.AttModel;
import com.bulsuclass.attendancemonitoring.models.Attendance;
import com.bulsuclass.attendancemonitoring.models.Student;
import com.bulsuclass.attendancemonitoring.models.Subject;
import com.bulsuclass.attendancemonitoring.services.DatabaseHelper;

import java.util.ArrayList;

public class AttendanceController {

    Context context;

    public AttendanceController(Context context) {
        this.context = context;
    }


    //TODO: adding attendance sequence
    public void createAttendance(ArrayList<Attendance> attendances) {

        DatabaseHelper helper = new DatabaseHelper(this.context);
        SQLiteDatabase db = helper.getWritables();

        //TODO: Check first if there is already an attendance today about the same class

        if(!hasAttendanceToday(helper, db, attendances.get(0))) {

            //TODO: Insert into attendance table the attendances ArrayList
            for(Attendance attendance : attendances) {
                addAttendance(helper, db, attendance);
            }
            Toast.makeText(context, "Attendance record added", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(context, "Has Attendance today", Toast.LENGTH_SHORT).show();
        }
        db.close();
    }

    // method for checking existing attendance in class
    private boolean hasAttendanceToday(DatabaseHelper helper, SQLiteDatabase db, Attendance attendance) {
        boolean hasAttendance = false;

        try {

            String query = "SELECT * FROM " + helper.ATTENDANCE_TABLE_NAME + " " +
                    "WHERE subject_id=? AND date=?";

            Subject subject = attendance.getSubject();

            Cursor cursor = db.rawQuery(query, new String[] { String.valueOf(subject.getId()), attendance.getDate() });
            hasAttendance = cursor.getCount() > 0 ? true : false;
        }
        catch(Exception e) {
            Toast.makeText(context, "Something went wrong!!", Toast.LENGTH_SHORT).show();
        }

        return hasAttendance;
    }

    private void addAttendance(DatabaseHelper helper, SQLiteDatabase db, Attendance attendance) {

        try {

            Student student = attendance.getStudent();
            Subject subject = attendance.getSubject();

            ContentValues contentValues = new ContentValues();
            contentValues.put(helper.ATTENDANCE_SUBJECT_ID, String.valueOf(subject.getId()));
            contentValues.put(helper.ATTENDANCE_STUDENT_ID, student.getId());
            contentValues.put(helper.ATTENDANCE_DATE, attendance.getDate());
            contentValues.put(helper.ATTENDANCE_ATTENDED, attendance.getAttended());

            db.insert(helper.ATTENDANCE_TABLE_NAME, null, contentValues);

        }
        catch(Exception e) {
            Toast.makeText(context, "Something went wrong!!", Toast.LENGTH_SHORT).show();
        }
    }

    public ArrayList<AttModel> getAttendances() {
        ArrayList<AttModel> attendances = new ArrayList<>();

        try {

            DatabaseHelper helper = new DatabaseHelper(this.context);
            SQLiteDatabase db = helper.getReadable();
            String query = "SELECT * FROM " + helper.ATTENDANCE_TABLE_NAME;

            Cursor cursor = db.rawQuery(query, null);

            while(cursor.moveToNext()) {

                String attendanceId = cursor.getString(cursor.getColumnIndex(helper.ATTENDANCE_ID));
                String subjectId = cursor.getString(cursor.getColumnIndex(helper.ATTENDANCE_SUBJECT_ID));
                String studentId = cursor.getString(cursor.getColumnIndex(helper.ATTENDANCE_STUDENT_ID));
                String attendanceDate = cursor.getString(cursor.getColumnIndex(helper.ATTENDANCE_DATE));
                String attended = cursor.getString(cursor.getColumnIndex(helper.ATTENDANCE_ATTENDED));
                String ip = cursor.getString(cursor.getColumnIndex(helper.ATTENDANCE_IP_ADDRESS));

                AttModel attendance = new AttModel(attendanceId, studentId, subjectId, attendanceDate, attended, ip);

                attendances.add(attendance);
            }
            db.close();
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        return attendances;
    }

    public void pushAddAttendance(DatabaseHelper helper, SQLiteDatabase db, ArrayList<Attendance> attendances) {

        try {

            ContentValues contentValues = new ContentValues();

            for(Attendance attendance : attendances) {

                Student student = attendance.getStudent();
                Subject subject = attendance.getSubject();
                String studentId = student.getId();
                String subjectId = String.valueOf(subject.getId());
                String date = attendance.getDate();
                String attended = attendance.getAttended();

                contentValues.put(helper.ATTENDANCE_STUDENT_ID, studentId);
                contentValues.put(helper.ATTENDANCE_SUBJECT_ID, subjectId);
                contentValues.put(helper.ATTENDANCE_DATE, date);
                contentValues.put(helper.ATTENDANCE_ATTENDED, attended);

                long result = db.insert(helper.ATTENDANCE_TABLE_NAME, null,  contentValues);

                if(result == -1) {
                    Toast.makeText(context, "Something went wrong!!", Toast.LENGTH_SHORT).show();
                }
            }

        }
        catch(Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Something went wrong!!", Toast.LENGTH_SHORT).show();
        }

    }

    public ArrayList<Attendance> getAttendanceHistory(Subject subject) {

        ArrayList<Attendance> attendances = new ArrayList<>();
        StudentController studentController = new StudentController(this.context);
        String subjectId = String.valueOf(subject.getId());
        try {

            DatabaseHelper helper = new DatabaseHelper(this.context);
            SQLiteDatabase db = helper.getReadable();

            String query = "SELECT * FROM " + helper.ATTENDANCE_TABLE_NAME +" " +
                    "WHERE " + helper.ATTENDANCE_SUBJECT_ID + "= ? " +
                    "GROUP BY " + helper.ATTENDANCE_DATE;

            Cursor cursor = db.rawQuery(query, new String[] { subjectId });
            while(cursor.moveToNext()) {
                String date = cursor.getString(cursor.getColumnIndex(helper.ATTENDANCE_DATE));
                String attended = cursor.getString(cursor.getColumnIndex(helper.ATTENDANCE_ATTENDED));
                String studentId = cursor.getString(cursor.getColumnIndex(helper.ATTENDANCE_STUDENT_ID));
                Student student = studentController.getStudentById(helper, db, studentId);
                attendances.add(new Attendance(null, student, subject, date, attended));
            }
            db.close();
        }
        catch(Exception e) {
            Toast.makeText(context, "Something went wrong!!", Toast.LENGTH_SHORT).show();
        }

        return attendances;
    }

    public ArrayList<Attendance> getAttendanceBySubjectAndDate(StudentController studentController, Subject subject, String date, String attended) {

        ArrayList<Attendance> attendances = new ArrayList<>();

        try {

            DatabaseHelper helper = new DatabaseHelper(this.context);
            SQLiteDatabase db = helper.getReadable();
            String query = "SELECT * FROM " + helper.ATTENDANCE_TABLE_NAME + " " +
                    "WHERE " + helper.ATTENDANCE_SUBJECT_ID + "=? " +
                    "AND " + helper.ATTENDANCE_DATE + "=? " +
                    "AND " + helper.ATTENDANCE_ATTENDED + "=?";

            if(attended.equals("yes")) {
                query = "SELECT * FROM " + helper.ATTENDANCE_TABLE_NAME + " " +
                        "WHERE " + helper.ATTENDANCE_SUBJECT_ID + "=? " +
                        "AND " + helper.ATTENDANCE_DATE + "=? " +
                        "AND " + helper.ATTENDANCE_ATTENDED + "=? OR " + helper.ATTENDANCE_ATTENDED + "=?";
            }

            String subjectId = String.valueOf(subject.getId());

            Cursor cursor = db.rawQuery(query, new String[] { subjectId, date, attended });

            if(attended.equals("yes")) {
                cursor = db.rawQuery(query, new String[] { subjectId, date, attended, "late" });
            }

            while(cursor.moveToNext()) {
                String attendanceId = cursor.getString(cursor.getColumnIndex(helper.ATTENDANCE_ID));
                String studentId = cursor.getString(cursor.getColumnIndex(helper.ATTENDANCE_STUDENT_ID));
                Student student = studentController.getStudentById(helper, db, studentId);
                String at = cursor.getString(cursor.getColumnIndex(helper.ATTENDANCE_ATTENDED));

                attendances.add(new Attendance(attendanceId, student, subject, date, at));
            }
            db.close();
        }
        catch(Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Something went wrong!!", Toast.LENGTH_SHORT).show();
        }

        return attendances;
    }

    public boolean updateAttendance(String subjectId, String studentId, String ip, String attendanceDate) {
        boolean hasUpdated = false;

        try {

            DatabaseHelper helper = new DatabaseHelper(this.context);
            SQLiteDatabase db = helper.getWritables();

            // TODO: Check first if student is registered to the subject, return false if not
            if(!isStudentRegistered(helper, db, subjectId, studentId)) {
                db.close();
                return false;
            }

            // TODO: Continue to update the Attendance table and set the attended to "yes"
            ContentValues contentValues = new ContentValues();
            contentValues.put(helper.ATTENDANCE_ATTENDED, "yes");
            contentValues.put(helper.ATTENDANCE_IP_ADDRESS, ip);

            long result = db.update(helper.ATTENDANCE_TABLE_NAME, contentValues,
                    helper.ATTENDANCE_SUBJECT_ID + "=? AND " +
                            helper.ATTENDANCE_STUDENT_ID + "=? " +
                            "AND " + helper.ATTENDANCE_DATE + "=? ", new String[] { subjectId, studentId, attendanceDate });

            hasUpdated = result != -1 ? true : false;

            db.close();

        }
        catch(Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Something went wrong!!", Toast.LENGTH_SHORT).show();
        }

        return hasUpdated;
    }

    public boolean deviceHadAttendance(String subjectId, String ip, String attendanceDate) {
        boolean hadAttendance = false;

        try {

            DatabaseHelper helper = new DatabaseHelper(this.context);
            SQLiteDatabase db = helper.getReadable();
            String query = "SELECT * FROM " + helper.ATTENDANCE_TABLE_NAME + " " +
                    "WHERE " + helper.ATTENDANCE_SUBJECT_ID + " = ? " +
                    "AND " + helper.ATTENDANCE_IP_ADDRESS + " = ? " +
                    "AND " + helper.ATTENDANCE_DATE + " = ?";

            Cursor result = db.rawQuery(query, new String[] { subjectId, ip, attendanceDate });
            hadAttendance = result.getCount() > 0 ? true : false;

            db.close();
        }
        catch(Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Something went wrong!!", Toast.LENGTH_SHORT).show();
        }

        return hadAttendance;
    }

    private boolean isStudentRegistered(DatabaseHelper helper, SQLiteDatabase db, String subjectId, String studentId) {
        boolean isRegistered = false;

        try {

            String query = "SELECT * FROM " + helper.ATTENDANCE_TABLE_NAME + " WHERE " +
                    "" + helper.ATTENDANCE_SUBJECT_ID + " = ? " +
                    "AND " + helper.ATTENDANCE_STUDENT_ID + " = ?";

            Cursor result = db.rawQuery(query, new String[] { subjectId, studentId });

            isRegistered = result.getCount() > 0 ? true : false;

        }
        catch(Exception e) {
            Toast.makeText(context, "Something went wrong!!", Toast.LENGTH_SHORT).show();
        }

        return isRegistered;
    }


    public boolean deleteAttendanceByDate(Subject subject, String date) {

        boolean hasDeleted = false;

        try {

            DatabaseHelper helper = new DatabaseHelper(this.context);
            SQLiteDatabase db = helper.getWritables();

            long result = db.delete(helper.ATTENDANCE_TABLE_NAME, helper.ATTENDANCE_SUBJECT_ID + "=? " +
                    "AND " + helper.ATTENDANCE_DATE + "=?", new String[] { String.valueOf(subject.getId()), date });

            hasDeleted = result == -1 ? false : true;
            db.close();

            if(hasDeleted)
                Toast.makeText(context, "Attendance deleted!", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(context, "Unable to delete attendance records", Toast.LENGTH_SHORT).show();
        }
        catch(Exception e) {
            Toast.makeText(context, "Something went wrong!!", Toast.LENGTH_SHORT).show();
        }

        return hasDeleted;
    }

    public void clearAttendances() {
        DatabaseHelper helper = new DatabaseHelper(this.context);
        SQLiteDatabase db = helper.getWritables();
        String query = "DELETE FROM " + helper.ATTENDANCE_TABLE_NAME;

        try {
            db.execSQL(query);

            db.close();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void lateStudent(String id) {
        DatabaseHelper helper = new DatabaseHelper(this.context);
        SQLiteDatabase db = helper.getWritables();

        ContentValues cv = new ContentValues();
        cv.put(helper.ATTENDANCE_ATTENDED, "late");
        try {
            db.update(helper.ATTENDANCE_TABLE_NAME, cv, helper.ATTENDANCE_ID + "=?", new String[] { id });
            db.close();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

}
