package com.bulsuclass.attendancemonitoring.controllers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.bulsuclass.attendancemonitoring.models.Classes;
import com.bulsuclass.attendancemonitoring.models.Subject;
import com.bulsuclass.attendancemonitoring.services.DatabaseHelper;

import java.util.ArrayList;

public class SubjectController {

    Context context;

    public SubjectController(Context context) {
        this.context = context;
    }

    public Boolean addSubject(Subject subject) {
        boolean hasAdded = false;
        DatabaseHelper helper = new DatabaseHelper(this.context);
        SQLiteDatabase db = helper.getWritables();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", subject.getName());
        contentValues.put("time", subject.getTime());

        try {
            String tableName = helper.SUBJECT_TABLE_NAME;
            long result = db.insert(tableName, null, contentValues);
            db.close();

            hasAdded = result == -1 ? false : true;
            if(result != -1)
                Toast.makeText(this.context, "Subject is added successfully!", Toast.LENGTH_LONG).show();
            else
                Toast.makeText(this.context, "Subject cannot be added!", Toast.LENGTH_LONG).show();
        }
        catch(Exception e) {
            Toast.makeText(this.context, "Something's wrong!!!", Toast.LENGTH_LONG).show();
        }

        return hasAdded;
    }

    public ArrayList<Subject> getSubjects() {

        ArrayList<Subject> subjects = new ArrayList<>();
        DatabaseHelper helper = new DatabaseHelper(this.context);
        SQLiteDatabase db = helper.getReadable();

        String query = "SELECT * FROM " + helper.SUBJECT_TABLE_NAME;

        try {
            Cursor cursor = db.rawQuery(query, null);
            while(cursor.moveToNext()) {
                int id = cursor.getInt(cursor.getColumnIndex("id"));
                String name = cursor.getString(cursor.getColumnIndex("name"));
                String time = cursor.getString(cursor.getColumnIndex("time"));

                subjects.add(new Subject(id, name, time));
            }
            db.close();
        }
        catch(Exception e) {
            Toast.makeText(this.context, "Something's wrong!!!", Toast.LENGTH_LONG).show();
        }

        return subjects;
    }

    public ArrayList<Classes> getClasses() {
        ArrayList<Classes> classes = new ArrayList<>();

        try {

            DatabaseHelper helper = new DatabaseHelper(this.context);
            SQLiteDatabase db = helper.getReadable();
            String query = "SELECT * FROM " + helper.CLASSES_TABLE_NAME;

            Cursor cursor = db.rawQuery(query, null);

            while(cursor.moveToNext()) {
                String classesId = cursor.getString(cursor.getColumnIndex(helper.CLASSES_ID));
                String subjectId = cursor.getString(cursor.getColumnIndex(helper.CLASSES_SUBJECT_ID));
                String studentId = cursor.getString(cursor.getColumnIndex(helper.CLASSES_STUDENT_ID));

                Classes _class = new Classes(classesId, subjectId, studentId);

                classes.add(_class);
            }
            db.close();
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        return classes;
    }

    public Subject getSubjectById(String id) {

        try {
            DatabaseHelper helper = new DatabaseHelper(this.context);
            SQLiteDatabase db = helper.getReadable();
            String query = "SELECT * FROM " + helper.SUBJECT_TABLE_NAME + " WHERE id=?";
            Subject subject = null;
            Cursor cursor = db.rawQuery(query, new String[] { id });
            if(cursor.moveToNext()) {
                int subject_id = cursor.getInt(cursor.getColumnIndex("id"));
                String subject_name = cursor.getString(cursor.getColumnIndex("name"));
                String subject_time = cursor.getString(cursor.getColumnIndex("time"));

                subject = new Subject(subject_id, subject_name, subject_time);
            }
            db.close();
            return subject;
        }
        catch(Exception e) {
            Toast.makeText(this.context, "Something's wrong!!!", Toast.LENGTH_LONG).show();
        }

        return new Subject();
    }

    public Boolean updateSubject(Subject subject) {
        boolean hasUpdated = false;

        try {

            DatabaseHelper helper = new DatabaseHelper(this.context);
            SQLiteDatabase db = helper.getWritables();
            ContentValues contentValues = new ContentValues();

            contentValues.put(helper.SUBJECT_NAME, subject.getName());
            contentValues.put(helper.SUBJECT_TIME, subject.getTime());

            long result = db.update(helper.SUBJECT_TABLE_NAME, contentValues, "id=?", new String[] { String.valueOf(subject.getId()) });

            hasUpdated = result == -1 ? false : true;
            db.close();

            if(hasUpdated)
                Toast.makeText(this.context, "Subject Updated!", Toast.LENGTH_LONG).show();
            else
                Toast.makeText(this.context, "Unable to update subject record", Toast.LENGTH_LONG).show();
        }
        catch(Exception e) {
            Toast.makeText(this.context, "Something's wrong!!!", Toast.LENGTH_LONG).show();
        }

        return hasUpdated;
    }

    public Boolean removeSubject(String id) {

        boolean hasDeleted = false;

        try {

            DatabaseHelper helper = new DatabaseHelper(this.context);
            SQLiteDatabase db = helper.getWritables();

            long result = db.delete(helper.SUBJECT_TABLE_NAME, "id=?", new String[]{ id });
            hasDeleted = result == -1 ? false : true;
            db.close();

            if(hasDeleted)
                Toast.makeText(context, "Subject deleted!", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(context, "Unable to delete the subject", Toast.LENGTH_SHORT).show();

        }
        catch (Exception e) {
            Toast.makeText(context, "Something went wrong!!", Toast.LENGTH_SHORT).show();
        }

        return hasDeleted;
    }

    public void clearSubjects() {
        DatabaseHelper helper = new DatabaseHelper(this.context);
        SQLiteDatabase db = helper.getWritables();
        String query = "DELETE FROM " + helper.SUBJECT_TABLE_NAME;

        try {

            db.execSQL(query);

            db.close();

        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void clearClasses() {
        DatabaseHelper helper = new DatabaseHelper(this.context);
        SQLiteDatabase db = helper.getWritables();
        String query = "DELETE FROM " + helper.CLASSES_TABLE_NAME;

        try {
            db.execSQL(query);

            db.close();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

}
