package com.bulsuclass.attendancemonitoring.controllers;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.bulsuclass.attendancemonitoring.models.AttModel;
import com.bulsuclass.attendancemonitoring.models.Classes;
import com.bulsuclass.attendancemonitoring.models.Student;
import com.bulsuclass.attendancemonitoring.models.Subject;
import com.bulsuclass.attendancemonitoring.services.DatabaseHelper;

import java.util.ArrayList;

public class BackupController {

    Context context;

    public BackupController(Context context) {
        this.context = context;
    }

    public void createBackup(ArrayList<Subject> subjects, ArrayList<Student> students, ArrayList<Classes> classes, ArrayList<AttModel> attendances) {
        DatabaseHelper helper = new DatabaseHelper(this.context);
        SQLiteDatabase db = helper.getWritables();
        ContentValues subject_cv = new ContentValues();
        ContentValues student_cv = new ContentValues();
        ContentValues classes_cv = new ContentValues();
        ContentValues attendance_cv = new ContentValues();

        try {

            for(int i = 0; i < subjects.size(); i++) {
                subject_cv.put("id", subjects.get(i).getId());
                subject_cv.put("name", subjects.get(i).getName());
                subject_cv.put("time", subjects.get(i).getTime());

                db.insert(helper.SUBJECT_TABLE_NAME, null, subject_cv);
            }

            for (int i = 0; i < students.size(); i++) {
                Student student = students.get(i);
                student_cv.put(helper.STUDENT_ID, student.getId());
                student_cv.put(helper.STUDENT_FIRST_NAME, student.getFirstName());
                student_cv.put(helper.STUDENT_LAST_NAME, student.getLastName());
                student_cv.put(helper.STUDENT_M_I, student.getMi());

                db.insert(helper.STUDENT_TABLE_NAME, null, student_cv);
            }

            for (int i = 0; i < classes.size(); i++) {
                Classes _class = classes.get(i);
                classes_cv.put(helper.CLASSES_ID, _class.getClassId());
                classes_cv.put(helper.CLASSES_SUBJECT_ID, _class.getClassSubjectId());
                classes_cv.put(helper.CLASSES_STUDENT_ID, _class.getClassStudentId());

                db.insert(helper.CLASSES_TABLE_NAME, null, classes_cv);
            }

            for (int i = 0; i < attendances.size(); i ++) {
                AttModel att = attendances.get(i);
                attendance_cv.put(helper.ATTENDANCE_ID, att.getId());
                attendance_cv.put(helper.ATTENDANCE_SUBJECT_ID, att.getSubjectId());
                attendance_cv.put(helper.ATTENDANCE_STUDENT_ID, att.getStudentId());
                attendance_cv.put(helper.ATTENDANCE_DATE, att.getDate());
                attendance_cv.put(helper.ATTENDANCE_ATTENDED, att.getAttended());
                attendance_cv.put(helper.ATTENDANCE_IP_ADDRESS, att.getIp());

                db.insert(helper.ATTENDANCE_TABLE_NAME, null, attendance_cv);
            }

            db.close();

        }
        catch(Exception e) {
            e.printStackTrace();
        }

    }
}
