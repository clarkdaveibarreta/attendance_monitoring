package com.bulsuclass.attendancemonitoring.controllers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

import com.bulsuclass.attendancemonitoring.models.Attendance;
import com.bulsuclass.attendancemonitoring.models.Student;
import com.bulsuclass.attendancemonitoring.models.Subject;
import com.bulsuclass.attendancemonitoring.services.DatabaseHelper;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class StudentController {

    private Context context;

    public StudentController(Context context) {
        this.context = context;
    }

    public boolean addStudentToClass(Student student, Subject subject) {

        DatabaseHelper helper = new DatabaseHelper(this.context);
        SQLiteDatabase db = helper.getWritables();

        // TODO: Check if student number exists in the Student table
        // TODO: Add student record if not exists

        if(!studentExists(db, student.getId())) {
            addStudent(db, helper, student);
        }

        //TODO: Check if student is in the attendance then add the student if he's not yet in the attendance
        if(subjectHasAttendance(helper, db, subject)) {
            if(!studentInAttendance(helper, db, student, subject)) {
                // Get all previous dates of attendance in this subject
                List<String> dateList = getPreviousAttendance(helper, db, subject);

                // Add student in attendance based on the given dates list
                if(dateList.size() > 0 && dateList != null) {

                    AttendanceController _con = new AttendanceController(this.context);
                    ArrayList<Attendance> attendances = new ArrayList<>();
                    for(String date : dateList) {
                        attendances.add(new Attendance(null, student, subject, date, "no"));
                    }
                    _con.pushAddAttendance(helper, db, attendances);
                }
            }
        }

        // TODO: Check if student was already in the class master list
        // TODO: Add student and subject to class record
        boolean result = false;
        if(!studentInClass(db, helper, student.getId(), String.valueOf(subject.getId()))) {
            result = addToClass(db, helper, student.getId(), String.valueOf(subject.getId()));

            if(result) {
                Toast.makeText(context, "Student Added to Class Success", Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(context, "Unable to add student to class", Toast.LENGTH_SHORT).show();
            }
        }
        else {
            Toast.makeText(context, "Student already in the class", Toast.LENGTH_SHORT).show();
        }

        db.close();

        return result;
    }

    public ArrayList<Student> getStudents() {

        ArrayList<Student> students = new ArrayList<>();
        DatabaseHelper helper = new DatabaseHelper(this.context);
        SQLiteDatabase db = helper.getReadable();

        String query = "SELECT * FROM " + helper.STUDENT_TABLE_NAME;

        try {
            Cursor cursor = db.rawQuery(query, null);

            while(cursor.moveToNext()) {
                String id = cursor.getString(cursor.getColumnIndex(helper.STUDENT_ID));
                String fname  = cursor.getString(cursor.getColumnIndex(helper.STUDENT_FIRST_NAME));
                String lname = cursor.getString(cursor.getColumnIndex(helper.STUDENT_LAST_NAME));
                String mi = cursor.getString(cursor.getColumnIndex(helper.STUDENT_M_I));

                students.add(new Student(id, fname, lname, mi));
            }
            db.close();
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        return students;
    }

    private boolean subjectHasAttendance(DatabaseHelper helper, SQLiteDatabase db, Subject subject) {
        boolean hasAttendance = false;

        try {
            String subjectId = String.valueOf(subject.getId());
            String query = "SELECT * FROM " + helper.ATTENDANCE_TABLE_NAME + " " +
                    "WHERE " + helper.ATTENDANCE_SUBJECT_ID + " = ? ";

            Cursor result = db.rawQuery(query, new String[] { subjectId });

            hasAttendance = result.getCount() > 0 ? true : false;

        }
        catch(Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Student already in the class", Toast.LENGTH_SHORT).show();
        }

        return hasAttendance;
    }

    private boolean studentInAttendance(DatabaseHelper helper, SQLiteDatabase db, Student student, Subject subject) {
        boolean hasAttendance = false;
        try {

            String subjectId = String.valueOf(subject.getId());
            String studentId = student.getId();
            String query = "SELECT * FROM " + helper.ATTENDANCE_TABLE_NAME + " " +
                    "WHERE " + helper.ATTENDANCE_SUBJECT_ID + " = ? " +
                    "AND " + helper.ATTENDANCE_STUDENT_ID + " = ?";

            Cursor result = db.rawQuery(helper.ATTENDANCE_TABLE_NAME, new String[] { subjectId, studentId });

            hasAttendance = result.getCount() > 0 ? true : false;

        }
        catch(Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Student already in the class", Toast.LENGTH_SHORT).show();
        }

        return hasAttendance;
    }

    private List<String> getPreviousAttendance(DatabaseHelper helper, SQLiteDatabase db, Subject subject) {
        List<String> dateList = new ArrayList<>();

        try {

            String subjectId = String.valueOf(subject.getId());
            String query = "SELECT * FROM " + helper.ATTENDANCE_TABLE_NAME + " " +
                    "WHERE " + helper.ATTENDANCE_SUBJECT_ID + " = ? " +
                    "GROUP BY " + helper.ATTENDANCE_DATE;

            Cursor result = db.rawQuery(query, new String[] { subjectId });

            if(result.getCount() > 0) {
                while(result.moveToNext()) {
                    String date = result.getString(result.getColumnIndex("date"));
                    dateList.add(date);
                }
            }

        }
        catch(Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Student already in the class", Toast.LENGTH_SHORT).show();
        }

        return dateList;
    }



    private boolean studentExists(SQLiteDatabase db, String studentId) {
        boolean exists = false;

        try {

            String query = "SELECT * FROM students WHERE id=?";
            Cursor cursor = db.rawQuery(query, new String[] { studentId });

            exists = cursor.getCount() > 0 ? true : false;
        }
        catch(Exception e) {
            Toast.makeText(context, "Something went wrong!!", Toast.LENGTH_SHORT).show();
        }

        return exists;
    }

    private void addStudent(SQLiteDatabase db, DatabaseHelper helper, Student student) {
        try {

            ContentValues contentValues = new ContentValues();

            contentValues.put(helper.STUDENT_ID, student.getId());
            contentValues.put(helper.STUDENT_FIRST_NAME, student.getFirstName());
            contentValues.put(helper.STUDENT_LAST_NAME, student.getLastName());
            contentValues.put(helper.STUDENT_M_I, student.getMi());

            long result = db.insert(helper.STUDENT_TABLE_NAME, null, contentValues);

            if(result != -1) {
                Toast.makeText(context, "New student record has been added", Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(context, "Unable to add new student record", Toast.LENGTH_SHORT).show();
            }
        }
        catch(Exception e) {
            Toast.makeText(context, "Something went wrong!!", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean addToClass(SQLiteDatabase db, DatabaseHelper helper, String studentId, String subjectId) {
        boolean hasAdded = false;
        try {
            ContentValues contentValues = new ContentValues();

            contentValues.put(helper.CLASSES_STUDENT_ID, studentId);
            contentValues.put(helper.CLASSES_SUBJECT_ID, subjectId);

            long result = db.insert(helper.CLASSES_TABLE_NAME, null, contentValues);

            hasAdded = result == -1 ? false : true;

        }
        catch(Exception e) {
            Toast.makeText(context, "Something went wrong!!", Toast.LENGTH_SHORT).show();
        }

        return hasAdded;
    }

    private boolean studentInClass(SQLiteDatabase db, DatabaseHelper helper, String studentId, String subjectId) {

        boolean exists = false;
        try {
            String query = "SELECT * FROM " + helper.CLASSES_TABLE_NAME +
                    " WHERE " + helper.CLASSES_STUDENT_ID + "=?"+
                    " AND " + helper.CLASSES_SUBJECT_ID + "=?";

            Cursor result = db.rawQuery(query, new String[] { studentId, subjectId });

            exists = result.getCount() > 0 ? true : false;
            Log.d("Number of rows", String.valueOf(result.getCount()));
        }
        catch(Exception e) {
            Toast.makeText(context, "Something went wrong!!", Toast.LENGTH_SHORT).show();
        }

        return exists;
    }

    public Student getStudentById(DatabaseHelper helper, SQLiteDatabase db, String id) {

        Student student = null;

        try {

            String query = "SELECT * FROM " + helper.STUDENT_TABLE_NAME + " WHERE id=?";
            Cursor cursor = db.rawQuery(query, new String[] { id });
            while(cursor.moveToNext()) {
                String studentId = cursor.getString(cursor.getColumnIndex(helper.STUDENT_ID));
                String firstName = cursor.getString(cursor.getColumnIndex(helper.STUDENT_FIRST_NAME));
                String lastName = cursor.getString(cursor.getColumnIndex(helper.STUDENT_LAST_NAME));
                String mi = cursor.getString(cursor.getColumnIndex(helper.STUDENT_M_I));

                student = new Student(studentId, firstName, lastName, mi);
            }
        }
        catch(Exception e) {
            Toast.makeText(context, "Something went wrong!!", Toast.LENGTH_SHORT).show();
        }

        return  student;
    }

    public ArrayList<Student> getStudentsByClass(String subjectId) {
        ArrayList<Student> students = new ArrayList<>();

        try {

            DatabaseHelper helper = new DatabaseHelper(this.context);
            SQLiteDatabase db = helper.getReadable();

            String query = "SELECT " +
                    helper.STUDENT_TABLE_NAME + "." + helper.STUDENT_ID + ", " +
                    helper.STUDENT_TABLE_NAME + "." + helper.STUDENT_FIRST_NAME + ", " +
                    helper.STUDENT_TABLE_NAME + "." + helper.STUDENT_LAST_NAME + ", " +
                    helper.STUDENT_TABLE_NAME + "." + helper.STUDENT_M_I +
                    " FROM " + helper.STUDENT_TABLE_NAME + " " +
                    " LEFT JOIN " + helper.CLASSES_TABLE_NAME + " " +
                    " ON " + helper.STUDENT_TABLE_NAME + "." + helper.STUDENT_ID +
                    " = " + helper.CLASSES_TABLE_NAME + "." + helper.CLASSES_STUDENT_ID +
                    " WHERE " + helper.CLASSES_SUBJECT_ID + "=?";

            Cursor cursor = db.rawQuery(query, new String[] { subjectId });
            while(cursor.moveToNext()) {
                String studentId = cursor.getString(0);
                String firstName = cursor.getString(1);
                String lastName = cursor.getString(2);
                String mi = cursor.getString(3);

                students.add(new Student(studentId, firstName, lastName, mi));
            }

        }
        catch(Exception e) {
            Toast.makeText(context, "Something went wrong!!", Toast.LENGTH_SHORT).show();
        }

        return students;
    }

    public boolean removeStudentFromClass(String studentId, String subjectId) {

        boolean removed = false;

        try {
            DatabaseHelper helper = new DatabaseHelper(this.context);
            SQLiteDatabase db = helper.getWritables();

            long result = db.delete(helper.CLASSES_TABLE_NAME, "student_id=? AND subject_id=?", new String[] { studentId, subjectId });

            removed = result == -1 ? false : true;

            if(removed) {
                removeStudentFromAttendance(helper, db, studentId, subjectId);
            }

            db.close();
        }
        catch(Exception e) {
            Toast.makeText(context, "Something went wrong!!", Toast.LENGTH_SHORT).show();
        }

        return removed;
    }

    private void removeStudentFromAttendance(DatabaseHelper helper, SQLiteDatabase db, String studentId, String subjectId) {

        try {

            String whereClause = helper.ATTENDANCE_STUDENT_ID + " = ? AND " +
                    helper.ATTENDANCE_SUBJECT_ID + " = ?";

            db.delete(helper.ATTENDANCE_TABLE_NAME, whereClause, new String[] { studentId, subjectId });

        }
        catch(Exception e) {
            Toast.makeText(context, "Something went wrong!!", Toast.LENGTH_SHORT).show();
        }

    }

    public void clearStudent() {

        DatabaseHelper helper = new DatabaseHelper(this.context);
        SQLiteDatabase db = helper.getWritables();
        String query = "DELETE FROM " + helper.STUDENT_TABLE_NAME;

        try {
            db.execSQL(query);

            db.close();
        }
        catch(Exception e) {
            e.printStackTrace();
        }

    }

}
