package com.bulsuclass.attendancemonitoring.pages;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bulsuclass.attendancemonitoring.R;
import com.bulsuclass.attendancemonitoring.controllers.AttendanceController;
import com.bulsuclass.attendancemonitoring.controllers.BackupController;
import com.bulsuclass.attendancemonitoring.controllers.StudentController;
import com.bulsuclass.attendancemonitoring.controllers.SubjectController;
import com.bulsuclass.attendancemonitoring.models.AttModel;
import com.bulsuclass.attendancemonitoring.models.Attendance;
import com.bulsuclass.attendancemonitoring.models.Classes;
import com.bulsuclass.attendancemonitoring.models.Student;
import com.bulsuclass.attendancemonitoring.models.Subject;
import com.bulsuclass.attendancemonitoring.services.DatabaseHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FetchBackup extends AppCompatActivity {

    Button fetchButton;

    String token;

    StudentController studentController;
    SubjectController subjectController;
    AttendanceController attendanceController;
    BackupController backupController;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fetch_backup);

        fetchButton = findViewById(R.id.fetch_button);

        fetchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back();
            }
        });

        SharedPreferences prefs = getSharedPreferences("PREFS", MODE_PRIVATE);
        token = prefs.getString("token", null);

        if(token.equals(null)) {
            startActivity(new Intent(this, LoginActivity.class));
            this.finish();
        }

        studentController = new StudentController(this);
        subjectController = new SubjectController(this);
        attendanceController = new AttendanceController(this);
        backupController = new BackupController(this);

        fetchBackup();
    }

    private void back() {
        startActivity(new Intent(this, HomeActivity.class));
        this.finish();
    }

    private void fetchBackup() {
        DatabaseHelper helper = new DatabaseHelper(this);
        String url = helper.API_URL + "/fetch_backup";
        fetchButton.setVisibility(View.INVISIBLE);

        RequestQueue q = Volley.newRequestQueue(this);

        StringRequest request = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("BACKUP DATA", response);
                        getResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + token);

                return headers;
            }
        };

        q.add(request);
    }

    private void getResponse(String res) {
        studentController.clearStudent();
        subjectController.clearSubjects();
        subjectController.clearClasses();
        attendanceController.clearAttendances();

        try {

            JSONObject response = new JSONObject(res);
            JSONArray subjects = response.getJSONArray("subjects");
            JSONArray students = response.getJSONArray("students");
            JSONArray classes = response.getJSONArray("classes");
            JSONArray attendances = response.getJSONArray("attendances");

            ArrayList<Subject> subjectList = getSubjects(subjects);
            ArrayList<Student> studentList = getStudents(students);
            ArrayList<Classes> classList = getClasses(classes);
            ArrayList<AttModel> attendanceList = getAttendances(attendances);

            backupController.createBackup(subjectList, studentList, classList, attendanceList);
            fetchButton.setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        fetchButton.setVisibility(View.VISIBLE);
    }

    private ArrayList<Subject> getSubjects(JSONArray subjects) {

        ArrayList<Subject> list = new ArrayList<>();

        try {

            for(int i = 0; i < subjects.length(); i++) {
                JSONObject subject = subjects.getJSONObject(i);
                int id = subject.getInt("subject_id");
                String name = subject.getString("subject_name");
                String time = subject.getString("subject_time");

                list.add(new Subject(id, name, time));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }

    private ArrayList<Student> getStudents(JSONArray students) {
        ArrayList<Student> list = new ArrayList<>();

        try {

            for(int i = 0; i < students.length(); i++) {
                JSONObject student = students.getJSONObject(i);
                String id = student.getString("student_id");
                String fname = student.getString("student_fname");
                String lname = student.getString("student_lname");
                String mi = student.getString("student_mi");

                list.add(new Student(id, fname, lname, mi));
            }

        }
        catch(JSONException e) {
            e.printStackTrace();
        }

        return list;
    }

    private ArrayList<Classes> getClasses(JSONArray classes) {
        ArrayList<Classes> list = new ArrayList<>();

        try {
            for(int i = 0; i < classes.length(); i++) {
                JSONObject _class = classes.getJSONObject(i);
                String id = _class.getString("classes_id");
                String subjectId = _class.getString("classes_subject_id");
                String studentId = _class.getString("classes_student_id");

                list.add(new Classes(id, subjectId, studentId));
            }
        }
        catch(JSONException e) {
            e.printStackTrace();
        }

        return list;
    }

    private ArrayList<AttModel> getAttendances(JSONArray attendances) {
        ArrayList<AttModel> list = new ArrayList<>();

        try {

            for(int i = 0; i < attendances.length(); i++) {
                JSONObject attendance = attendances.getJSONObject(i);
                String id = attendance.getString("attendance_id");
                String subjectId = attendance.getString("attendance_subject_id");
                String studentId = attendance.getString("attendance_student_id");
                String date = attendance.getString("attendance_date");
                String attended = attendance.getString("attendance_attended");
                String ip = attendance.getString("attendance_ip_address");

                list.add(new AttModel(id, studentId, subjectId, date, attended, ip));
            }

        }
        catch(JSONException e) {
            e.printStackTrace();
        }

        return list;
    }
}