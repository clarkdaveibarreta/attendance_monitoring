package com.bulsuclass.attendancemonitoring.pages;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bulsuclass.attendancemonitoring.R;
import com.bulsuclass.attendancemonitoring.controllers.StudentController;
import com.bulsuclass.attendancemonitoring.controllers.SubjectController;
import com.bulsuclass.attendancemonitoring.helpers.AddStudentDialog;
import com.bulsuclass.attendancemonitoring.helpers.UpdateSubjectDialog;
import com.bulsuclass.attendancemonitoring.models.Student;
import com.bulsuclass.attendancemonitoring.models.Subject;

import java.util.ArrayList;

public class ClassesActivity extends AppCompatActivity implements UpdateSubjectDialog.UpdateSubjectDialogListener, AddStudentDialog.AddStudentDialogListener {

    private String subject_id;
    private ArrayList<Student> students;

    private SubjectController subjectController;
    private StudentController studentController;

    TextView textViewSubjctName, textViewSubjectTime;
    Button editSubjectButton, addStudentButton;
    ListView studentListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classes);

        // Get Intent
        Intent intent = getIntent();
        subject_id = intent.getStringExtra(SubjectActivity.SUBJECT_ID_EXTRA);

        // Initialize Controllers
        subjectController = new SubjectController(ClassesActivity.this);
        studentController = new StudentController(ClassesActivity.this);

        // Initialize Views
        textViewSubjctName = findViewById(R.id.text_view_subject_name);
        textViewSubjectTime = findViewById(R.id.text_view_subject_time);
        editSubjectButton = findViewById(R.id.edit_subject_button);
        addStudentButton = findViewById(R.id.add_student_button);
        studentListView = findViewById(R.id.student_list_view);

        setInitialValues();
        setButtonControls();
        populateStudentListView();
        setListViewControls();
    }

    private void setInitialValues() {
        Subject subject = subjectController.getSubjectById(subject_id);

        textViewSubjctName.setText(subject.getName());
        textViewSubjectTime.setText(subject.getTime());
    }

    private void setButtonControls() {
        editSubjectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: create process for editing the subject
                updateSubject();
            }
        });

        addStudentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: create process for adding a student in class
                AddStudentDialog addStudentDialog = new AddStudentDialog();
                addStudentDialog.show(getSupportFragmentManager(), "add student");
            }
        });
    }

    private void setListViewControls() {
        studentListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                removeStudentFromClass(position);
                return true;
            }
        });
    }

    private void populateStudentListView() {
        this.students = studentController.getStudentsByClass(this.subject_id);
        ArrayList<String> studentList = new ArrayList<>();

        for(int i = 0; i < students.size(); i++) {

            String id = students.get(i).getId();
            String fn = students.get(i).getFirstName();
            String ln = students.get(i).getLastName();
            String mi = students.get(i).getMi();

            studentList.add("(" + id + ") " + ln + ", " + fn + " " + mi + ".");
        }

        ArrayAdapter adapter = new ArrayAdapter(ClassesActivity.this, android.R.layout.simple_list_item_1, studentList);
        studentListView.setAdapter(adapter);
    }

    private void updateSubject() {
        Subject subject = subjectController.getSubjectById(subject_id);
        UpdateSubjectDialog updateSubjectDialog = new UpdateSubjectDialog(subject);
        updateSubjectDialog.show(getSupportFragmentManager(), "Update Subject");
    }

    private void removeStudentFromClass(int position) {
        String studentId = this.students.get(position).getId();

        AlertDialog.Builder builder = new AlertDialog.Builder(ClassesActivity.this);
        builder.setTitle("Remove Student")
                .setMessage("are you sure you want to remove this student from class ?")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Cancel the Dialog
                    }
                })
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(studentController.removeStudentFromClass(studentId, subject_id)) {
                            Toast.makeText(ClassesActivity.this, "Student removed from class successfully", Toast.LENGTH_SHORT).show();
                            populateStudentListView();
                        }
                        else {
                            Toast.makeText(ClassesActivity.this, "Unable to remove student from class", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .show();
    }

    @Override
    public void updateSubject(Subject subject) {
        if(subjectController.updateSubject(subject))
            setInitialValues();
    }

    @Override
    public void AddStudent(Student student) {

        Subject subject = subjectController.getSubjectById(subject_id);
        if(studentController.addStudentToClass(student, subject)) {
            populateStudentListView();
        }

    }
}