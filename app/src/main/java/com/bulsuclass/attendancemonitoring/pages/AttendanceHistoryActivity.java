package com.bulsuclass.attendancemonitoring.pages;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.bulsuclass.attendancemonitoring.MainActivity;
import com.bulsuclass.attendancemonitoring.R;
import com.bulsuclass.attendancemonitoring.controllers.AttendanceController;
import com.bulsuclass.attendancemonitoring.controllers.StudentController;
import com.bulsuclass.attendancemonitoring.controllers.SubjectController;
import com.bulsuclass.attendancemonitoring.models.Attendance;
import com.bulsuclass.attendancemonitoring.models.Student;
import com.bulsuclass.attendancemonitoring.models.Subject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class AttendanceHistoryActivity extends AppCompatActivity {

    public static final String ATTENDACE_SUBJECT_ID_EXTRA = "com.bulsuclass.attendancemonitoring.ATTENDACE_SUBJECT_ID_EXTRA";
    public static final String ATTENDANCE_DATE = "com.bulsuclass.attendancemonitoring.ATTENDANCE_DATE";

    private String subjectId;

    private Subject subject;
    private ArrayList<Student> students;
    ArrayList<Attendance> attendances;
    private AttendanceController attendanceController;
    private SubjectController subjectController;
    private StudentController studentController;


    ListView attendanceListView;
    Button addAttendanceButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_history);

        // Initialize subjectId from intent
        Intent intent = getIntent();
        subjectId = intent.getStringExtra(AttendanceActivity.ATTENDANCE_SUBJECT_ID_EXTRA);

        // Initialize ListView
        attendanceListView = findViewById(R.id.attendance_history_list_view);
        addAttendanceButton = findViewById(R.id.create_attendance_button);

        // Initialize Values
        setInitialValues();

        // Setup controls
        setupButtonControls();

    }

    private void setInitialValues() {

        subjectController = new SubjectController(AttendanceHistoryActivity.this);
        attendanceController = new AttendanceController(AttendanceHistoryActivity.this);
        studentController = new StudentController(AttendanceHistoryActivity.this);

        subject = subjectController.getSubjectById(subjectId);
        students = studentController.getStudentsByClass(subjectId);
        setListView();
        setListViewControls();
    }


    private void setupButtonControls() {
        addAttendanceButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                createAttendance();
            }
        });
    }

    private void setListView() {
        attendances = attendanceController.getAttendanceHistory(subject);
        ArrayList<String> attendanceList = new ArrayList<>();

        for(Attendance attendance : attendances) {
            attendanceList.add(attendance.getDate());
        }
        ArrayAdapter adapter = new ArrayAdapter(AttendanceHistoryActivity.this, android.R.layout.simple_list_item_1, attendanceList);

        attendanceListView.setAdapter(adapter);
    }

    private void setListViewControls() {
        attendanceListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                deleteAttendance(position);

                return true;
            }
        });

        attendanceListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //TODO: open StudentAttendanceActivity with Subject ID and date as intent extra
                String date = attendances.get(position).getDate();

                Intent intent = new Intent(AttendanceHistoryActivity.this, StudentAttendanceActivity.class);
                intent.putExtra(ATTENDACE_SUBJECT_ID_EXTRA, subjectId);
                intent.putExtra(ATTENDANCE_DATE, date);
                startActivity(intent);
            }
        });
    }

    private void deleteAttendance(int position) {
        Attendance attendance = attendances.get(position);

        AlertDialog.Builder builder = new AlertDialog.Builder(AttendanceHistoryActivity.this);
        builder.setTitle("Delete Attendance")
                .setMessage("Do you want to delete this attendance record ?")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Cancels the dialog
                    }
                })
                .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(attendanceController.deleteAttendanceByDate(attendance.getSubject(), attendance.getDate())) {
                            setListView();
                        }
                    }
                })
                .show();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void createAttendance() {
        Calendar calendar = Calendar.getInstance();
        Date date = calendar.getTime();

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String dateString = sdf.format(date);
        String attended = "no";

        ArrayList<Attendance> attendances = new ArrayList<>();

        if(students.size() > 0) {
            for(Student student : students) {
                attendances.add(new Attendance(null, student, subject, dateString, attended));
            }

            attendanceController.createAttendance(attendances);
            setListView();
        }
        else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("No Enrolled Student")
                    .setMessage("Would you like to enroll student(s) in this class ?")
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(AttendanceHistoryActivity.this, AttendanceActivity.class));
                        }
                    })
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            startActivity(new Intent(AttendanceHistoryActivity.this, HomeActivity.class));
                        }
                    })
                    .show();
        }


    }

}