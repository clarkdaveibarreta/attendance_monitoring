package com.bulsuclass.attendancemonitoring.pages;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bulsuclass.attendancemonitoring.R;
import com.bulsuclass.attendancemonitoring.services.DatabaseHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    EditText idNumber, name, email, password, passwordConfirm;
    Button loginButton, registerStudent, registerTeacher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        initVariables();

    }

    private void initVariables() {

        idNumber = findViewById(R.id.register_id);
        name = findViewById(R.id.register_name);
        email = findViewById(R.id.register_email);
        password = findViewById(R.id.register_password);
        passwordConfirm = findViewById(R.id.passwordConfirm);

        loginButton = findViewById(R.id.register_log_button);
        registerTeacher = findViewById(R.id.register_teacher);
        registerStudent = findViewById(R.id.register_student);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

        registerTeacher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                teacher();
            }
        });

        registerStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                student();
            }
        });
    }

    private void login() {
        startActivity(new Intent(this, LoginActivity.class));
        this.finish();
    }

    private void teacher() {
        DatabaseHelper helper = new DatabaseHelper(this);
        String url = helper.API_URL + "/register";

        String _idNumber = idNumber.getText().toString();
        String _name = name.getText().toString();
        String _email = email.getText().toString();
        String _password = password.getText().toString();
        String _passwordConfirm = passwordConfirm.getText().toString();
        String _role = "teacher";

        RequestQueue q = Volley.newRequestQueue(this);

        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("USER", response);
                        try {
                            JSONObject res = new JSONObject(response);
                            JSONObject user = res.getJSONObject("user");
                            String id = user.getString("id");
                            String id_number = user.getString("id_number");
                            String name = user.getString("name");
                            String email = user.getString("email");
                            String role = user.getString("role");
                            String token = res.getString("token");

                            SharedPreferences prefs = getSharedPreferences("PREFS", MODE_PRIVATE);
                            SharedPreferences.Editor editor = prefs.edit();

                            editor.putString("user.id", id);
                            editor.putString("user.id_number", id_number);
                            editor.putString("user.name", name);
                            editor.putString("user.email", email);
                            editor.putString("user.role", role);
                            editor.putString("token", token);

                            editor.apply();

                            register_r(true);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        register_r(false);
                    }
                }){
            @Nullable
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id_number", _idNumber);
                params.put("name", _name);
                params.put("email", _email);
                params.put("password", _password);
                params.put("password_confirmation", _passwordConfirm);
                params.put("role", _role);

                return params;
            }
        };

        q.add(request);
    }

    private void student() {
        DatabaseHelper helper = new DatabaseHelper(this);
        String url = helper.API_URL + "/register";

        String _idNumber = idNumber.getText().toString();
        String _name = name.getText().toString();
        String _email = email.getText().toString();
        String _password = password.getText().toString();
        String _passwordConfirm = passwordConfirm.getText().toString();
        String _role = "student";

        RequestQueue q = Volley.newRequestQueue(this);

        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("USER", response);
                        try {
                            JSONObject res = new JSONObject(response);
                            JSONObject user = res.getJSONObject("user");
                            String id = user.getString("id");
                            String id_number = user.getString("id_number");
                            String name = user.getString("name");
                            String email = user.getString("email");
                            String role = user.getString("role");
                            String token = res.getString("token");

                            SharedPreferences prefs = getSharedPreferences("PREFS", MODE_PRIVATE);
                            SharedPreferences.Editor editor = prefs.edit();

                            editor.putString("user.id", id);
                            editor.putString("user.id_number", id_number);
                            editor.putString("user.name", name);
                            editor.putString("user.email", email);
                            editor.putString("user.role", role);
                            editor.putString("token", token);

                            editor.apply();

                            register_r(true);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        register_r(false);
                    }
                }){
            @Nullable
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("id_number", _idNumber);
                params.put("name", _name);
                params.put("email", _email);
                params.put("password", _password);
                params.put("password_confirmation", _passwordConfirm);
                params.put("role", _role);

                return params;
            }
        };

        q.add(request);
    }

    private void register_r(boolean response) {

        if(response) {
            startActivity(new Intent(this, HomeActivity.class));
            this.finish();
        }
        else {
            Toast.makeText(getBaseContext(), "Failed", Toast.LENGTH_SHORT).show();
        }

    }
}