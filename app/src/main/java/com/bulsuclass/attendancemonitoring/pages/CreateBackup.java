package com.bulsuclass.attendancemonitoring.pages;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bulsuclass.attendancemonitoring.R;
import com.bulsuclass.attendancemonitoring.controllers.AttendanceController;
import com.bulsuclass.attendancemonitoring.controllers.StudentController;
import com.bulsuclass.attendancemonitoring.controllers.SubjectController;
import com.bulsuclass.attendancemonitoring.models.AttModel;
import com.bulsuclass.attendancemonitoring.models.Classes;
import com.bulsuclass.attendancemonitoring.models.Student;
import com.bulsuclass.attendancemonitoring.models.Subject;
import com.bulsuclass.attendancemonitoring.services.DatabaseHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CreateBackup extends AppCompatActivity {

    Button doneButton;

    DatabaseHelper helper;
    SubjectController subjectController;
    StudentController studentController;
    AttendanceController attendanceController;

    String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_backup);

        SharedPreferences prefs = getSharedPreferences("PREFS", MODE_PRIVATE);
        token = prefs.getString("token", null);

        if(token.equals(null)) {
            startActivity(new Intent(this, LoginActivity.class));
            this.finish();
        }

        doneButton = findViewById(R.id.finish_backup);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back();

            }
        });

        helper = new DatabaseHelper(this);
        subjectController = new SubjectController(this);
        studentController = new StudentController(this);
        attendanceController = new AttendanceController(this);

        makeBackup();
    }

    private void back() {
        startActivity(new Intent(this, HomeActivity.class));
        this.finish();
    }

    private void lastBackup() {
        doneButton.setVisibility(View.VISIBLE);
    }

    private void makeBackup() {

        doneButton.setVisibility(View.INVISIBLE);

        ArrayList<Subject> subjects = subjectController.getSubjects();
        ArrayList<Student> students = studentController.getStudents();
        ArrayList<AttModel> attendances = attendanceController.getAttendances();
        ArrayList<Classes> classes = subjectController.getClasses();

        for(int i = 0; i < subjects.size(); i++) {
            subjectBackup(subjects.get(i));
        }

        for(int i = 0; i < students.size(); i++) {
            studentBackup(students.get(i));
        }

        for(int i = 0; i < attendances.size(); i++) {
            attendanceBackup(attendances.get(i));
        }

        for(int i = 0; i < classes.size(); i++) {
            classesBackup(classes.get(i));
        }

    }

    private void classesBackup(Classes _class) {
        String url = helper.API_URL + "/classroom_backup";

        RequestQueue q = Volley.newRequestQueue(this);
        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        lastBackup();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + token);

                return headers;
            }

            @Nullable
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("classes_id", _class.getClassId());
                params.put("classes_subject_id", _class.getClassSubjectId());
                params.put("classes_student_id", _class.getClassStudentId());

                return params;
            }
        };

        q.add(request);
    }

    private void attendanceBackup(AttModel attendance) {

        String url = helper.API_URL + "/attendance_backup";

        RequestQueue q = Volley.newRequestQueue(this);

        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("ATTENDANCE BACKUP", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + token);

                return headers;
            }

            @Nullable
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("attendance_id", attendance.getId());
                params.put("attendance_subject_id", attendance.getSubjectId());
                params.put("attendance_student_id", attendance.getStudentId());
                params.put("attendance_date", attendance.getDate());
                params.put("attendance_attended", attendance.getAttended());
                params.put("attendance_ip_address", attendance.getIp());

                return params;
            }
        };

        q.add(request);

    }

    private void subjectBackup(Subject subject) {

        String url = helper.API_URL + "/subject_backup";

        RequestQueue q = Volley.newRequestQueue(this);

        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("CREATED SUBJECTS BACKUP", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + token);
                headers.put("Accept", "application/json");
                return headers;
            }

            @Nullable
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("subject_id", String.valueOf(subject.getId()));
                params.put("subject_name", subject.getName());
                params.put("subject_time", subject.getTime());

                return params;
            }
        };

        q.add(request);

    }

    private void studentBackup(Student student) {

        String url = helper.API_URL + "/student_backup";

        RequestQueue q = Volley.newRequestQueue(this);

        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("STUDENT CREATED ", response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                }){
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + token);

                return headers;
            }

            @Nullable
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("student_id", student.getId());
                params.put("student_fname", student.getFirstName());
                params.put("student_lname", student.getLastName());
                params.put("student_mi", student.getMi());

                return params;
            }
        };

        q.add(request);

    }
}