package com.bulsuclass.attendancemonitoring.pages;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricPrompt;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.biometrics.BiometricManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bulsuclass.attendancemonitoring.R;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import android.provider.Settings.Secure;

public class    ConnectActivity extends AppCompatActivity {

    EditText serverIp, studentNumber;
    Button connectButton, submitButton;

    public static final int SERVER_PORT = 3004;
    private String SERVER_IP;
    private String IP_ADDRESS;
    private ClientThread clientThread;
    private Thread thread;
    private Handler handler;

    private Executor executor;
    private BiometricPrompt biometricPrompt;
    private BiometricPrompt.PromptInfo promptInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect);

        // Initialize Views
        initializeViews();

        // Initialize variables
        initializeVariables();

        // Initialize button controls
        initializeButtonControls();


//        executor = ContextCompat.getMainExecutor(this);
//        biometricPrompt = new BiometricPrompt(ConnectActivity.this,
//                executor, new BiometricPrompt.AuthenticationCallback() {
//            @Override
//            public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
//                super.onAuthenticationError(errorCode, errString);
//                Toast.makeText(getApplicationContext(), "Authentication error: " + errString, Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result) {
//                super.onAuthenticationSucceeded(result);
//                signAttendance();
//            }
//
//            @Override
//            public void onAuthenticationFailed() {
//                super.onAuthenticationFailed();
//                Toast.makeText(getApplicationContext(), "Authentication failed", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        promptInfo = new BiometricPrompt.PromptInfo.Builder()
//                .setTitle("Fingerprint Attendance")
//                .setSubtitle("Use fingerprint to sign in the attendance")
//                .setNegativeButtonText("Cancel")
//                .build();


        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signAttendance();
//                biometricPrompt.authenticate(promptInfo);
            }
        });


    }


    /******************************************
     *
     *  Initialize all the views variables in
     *  this method
     *
     ******************************************/
    private void initializeViews() {
        serverIp = findViewById(R.id.edit_text_server_ip);
        studentNumber = findViewById(R.id.edit_send_student_number);
        connectButton = findViewById(R.id.connect_to_server_button);
        submitButton = findViewById(R.id.submit_button);
    }

    /*********************************************
     *
     *  Initialize variables here in this method
     *
     */
    private void initializeVariables() {

        handler = new Handler();
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
//        IP_ADDRESS = Formatter.formatIpAddress(wifiInfo.getIpAddress());
//        IP_ADDRESS = wifiInfo.getMacAddress();
        IP_ADDRESS = Secure.getString(getApplicationContext().getContentResolver(), Secure.ANDROID_ID);
        Log.d("MAC_ADDRESS", IP_ADDRESS);
//        9b698670938ef8d3
    }


    /*********************************************
     *
     *  Initialize all the button controls in this
     *  method
     *
     ********************************************/
    private void initializeButtonControls() {
        connectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connectToServer();
            }
        });
    }

    public void signAttendance() {

        String studentNumber = this.studentNumber.getText().toString();
        String modifier = "%,%";
        String ipAddress = IP_ADDRESS;

        String message = studentNumber + modifier + ipAddress;

        if(thread != null && clientThread != null) {
            clientThread.sendMessage(message);
        }
        else {
            Toast.makeText(this, "Connect to a server First please", Toast.LENGTH_SHORT).show();
        }

    }

    public void showMessage(final String message) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(ConnectActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void connectToServer() {
        String ip = serverIp.getText().toString();
        SERVER_IP = ip;
        clientThread = new ClientThread();
        thread = new Thread(clientThread);
        thread.start();
        if(ip != "" && clientThread.socket != null) {
            showMessage("Connected to Server...");
        }
    }

    class ClientThread implements Runnable {

        private Socket socket;
        private BufferedReader input;

        @Override
        public void run() {
            try {

                InetAddress serverAddress = InetAddress.getByName(SERVER_IP);
                socket = new Socket(serverAddress, SERVER_PORT);

                while(!Thread.currentThread().isInterrupted()) {
                    this.input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    String message = input.readLine();

                    if(message == null || "Disconnect".contentEquals(message)) {
                        Thread.interrupted();
                        message = "Server Disconnected. ";
                        showMessage(message);
                        break;
                    }

                    showMessage(message);

                }
            }
            catch(Exception e) {
               e.printStackTrace();
               showMessage(e.getMessage());
            }
        }

        void sendMessage(final String message) {

            if(socket == null && socket.isClosed()) {
                showMessage("Not connected to any server");
                return;
            }

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if(socket != null && !socket.isClosed()) {
                            PrintWriter out = new PrintWriter(new BufferedWriter(
                                    new OutputStreamWriter(socket.getOutputStream())), true);

                            out.println(message);
                        }
                    }
                    catch(Exception e) {
                        e.printStackTrace();
                        showMessage(e.getMessage());
                    }
                }
            }).start();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(clientThread != null) {
            clientThread.sendMessage("Disconnect");
            clientThread = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(clientThread != null) {
            clientThread.sendMessage("Disconnect");
            clientThread = null;
        }
    }
}