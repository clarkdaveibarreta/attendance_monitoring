package com.bulsuclass.attendancemonitoring.pages;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.bulsuclass.attendancemonitoring.R;
import com.bulsuclass.attendancemonitoring.controllers.SubjectController;
import com.bulsuclass.attendancemonitoring.helpers.AddSubjectDialog;
import com.bulsuclass.attendancemonitoring.models.Subject;

import java.util.ArrayList;

public class SubjectActivity extends AppCompatActivity implements AddSubjectDialog.AddSubjectDialogListener {

    public final static String SUBJECT_ID_EXTRA = "com.bulsuclass.attendancemonitoring.SUBJECT_ID_EXTRA";

    ListView subjectListView;
    Button subjectAddButton;

    SubjectController con;
    ArrayList<Subject> subjects;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subject);

        //Initialize controller
        con = new SubjectController(SubjectActivity.this);
        // Initialize views
        subjectListView = findViewById(R.id.subject_list_view);
        subjectAddButton = findViewById(R.id.add_subject_button);

        subjectAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddSubjectModal();
            }
        });


        // populate ListView
        populateSubjectListView();
        setupListViewControls();
    }

    private void populateSubjectListView() {
        subjects = this.con.getSubjects();
        ArrayList<String> listViewTitle = new ArrayList<>();

        for(int i = 0; i < subjects.size(); i++) {
            listViewTitle.add(subjects.get(i).getName() + " (" + subjects.get(i).getTime() + ")");
        }

        if(listViewTitle.size() > 0) {
            ArrayAdapter adapter = new ArrayAdapter(SubjectActivity.this, android.R.layout.simple_list_item_1, listViewTitle);
            this.subjectListView.setAdapter(adapter);
        }

    }

    private void setupListViewControls() {
        this.subjectListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                openAddStudentActivity(position);
            }
        });

        this.subjectListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                deleteSubject(position);
                return true;
            }
        });
    }

    private void openAddStudentActivity(int position) {
        //TODO: open class members activity
        Subject subject = con.getSubjectById(String.valueOf(subjects.get(position).getId()));

        Intent intent = new Intent(SubjectActivity.this, ClassesActivity.class);
        intent.putExtra(SUBJECT_ID_EXTRA, String.valueOf(subject.getId()));
        startActivity(intent);
    }

    private void openAddSubjectModal() {
        AddSubjectDialog addSubjectDialog = new AddSubjectDialog();
        addSubjectDialog.show(getSupportFragmentManager(), "Add Subject");
    }

    @Override
    public void saveSubject(Subject subject) {
        if(this.con.addSubject(subject)) {
            populateSubjectListView();
        }
    }

    private void deleteSubject(int position) {

        String id = String.valueOf(subjects.get(position).getId());

        AlertDialog.Builder builder = new AlertDialog.Builder(SubjectActivity.this);
        builder.setTitle("Delete Subject")
                .setMessage("Do you want to delete this Subject ?")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Cancel the dialog
                    }
                })
                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        con.removeSubject(id);
                        populateSubjectListView();
                    }
                })
                .show();

    }
}