package com.bulsuclass.attendancemonitoring.pages;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.bulsuclass.attendancemonitoring.R;
import com.bulsuclass.attendancemonitoring.controllers.SubjectController;
import com.bulsuclass.attendancemonitoring.models.Subject;

import java.util.ArrayList;

public class AttendanceActivity extends AppCompatActivity {

    public static final String ATTENDANCE_SUBJECT_ID_EXTRA = "com.bulsuclass.attendancemonitoring.ATTENDANCE_SUBJECT_ID_EXTRA";

    ListView attendanceSubjectListView;

    ArrayList<Subject> subjects;
    SubjectController subjectController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);

        // Initialize attendance subject list view
        attendanceSubjectListView = findViewById(R.id.attendance_subject_list_view);

        // Initialize controllers
        subjectController = new SubjectController(AttendanceActivity.this);

        populateListView();
        setupListViewControls();

    }

    private void populateListView() {
        subjects = subjectController.getSubjects();
        ArrayList<String> subjectItem = new ArrayList<>();

        for(Subject subject : subjects) {
            subjectItem.add(subject.getName() + " ( "+ subject.getTime() +" )");
        }

        ArrayAdapter adapter = new ArrayAdapter(AttendanceActivity.this, android.R.layout.simple_list_item_1, subjectItem);
        attendanceSubjectListView.setAdapter(adapter);
    }

    private void setupListViewControls() {
        attendanceSubjectListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //TODO: Open attendance history activity with subject id as extra

                String subjectId = String.valueOf(subjects.get(position).getId());

                Intent intent = new Intent(AttendanceActivity.this, AttendanceHistoryActivity.class);
                intent.putExtra(ATTENDANCE_SUBJECT_ID_EXTRA, subjectId);
                startActivity(intent);
            }
        });
    }
}