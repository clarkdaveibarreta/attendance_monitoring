package com.bulsuclass.attendancemonitoring.pages;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.bulsuclass.attendancemonitoring.R;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeActivity extends AppCompatActivity {

    private Button classesButton, attendanceButton, connectButton, createBackup, fetchBackup;
    private ImageButton signoutButton;
    private CircleImageView signout;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
//        setContentView(R.layout.activity_home);

        classesButton = findViewById(R.id.classes_button);
        attendanceButton = findViewById(R.id.attendance_button);
        connectButton = findViewById(R.id.connect_button);
        signoutButton = findViewById(R.id.exitbutton);
        signout = (CircleImageView) findViewById(R.id.bubuy);

        createBackup = findViewById(R.id.create_backup);
        fetchBackup = findViewById(R.id.fetch_backup);

        SharedPreferences prefs = getSharedPreferences("PREFS", MODE_PRIVATE);
        String role = prefs.getString("user.role", null);

        if(role == null) {
            startActivity(new Intent(this, LoginActivity.class));
            this.finish();
        }

        Log.d("ROLE", role);

        if(role.equals("teacher")) {
            Log.d("ROLE", role);
            connectButton.setVisibility(View.INVISIBLE);
        }
        else if(role.equals("student")) {
            classesButton.setVisibility(View.INVISIBLE);
            attendanceButton.setVisibility(View.INVISIBLE);
            createBackup.setVisibility(View.INVISIBLE);
            fetchBackup.setVisibility(View.INVISIBLE);
        }

        classesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openClassesActivity();
            }
        });

        attendanceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAttendanceActivity();
            }
        });

        connectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openConnectActivity();
            }
        });

        createBackup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createBackUp();
            }
        });

        fetchBackup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBackuo();
            }
        });

        signoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences prefs = getSharedPreferences("PREFS", MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.clear();
                editor.commit();
                editor.apply();

                startActivity(new Intent(HomeActivity.this, LoginActivity.class));
                HomeActivity.this.finish();
            }
        });

    }

    private void getBackuo() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Fetch Backup")
                .setMessage("Current local data will be delete, are you sure ?")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        
                    }
                })
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        go();
                    }
                })
                .show();

    }

    private void go() {
        startActivity(new Intent(this, FetchBackup.class));
    }

    private void createBackUp() {

        startActivity(new Intent(this, CreateBackup.class));
    }

    private void openClassesActivity() {
        //TODO: open CLassesActivity here ...
        startActivity(new Intent(HomeActivity.this, SubjectActivity.class));
    }

    private void openAttendanceActivity() {
        //TODO: open JoinActivity here ...
        startActivity(new Intent(HomeActivity.this, AttendanceActivity.class));
    }

    private void openConnectActivity() {
        //TODO: open ConnectActivity here ...
        startActivity(new Intent(HomeActivity.this, ConnectActivity.class));
    }

    private void gotoMainActivity() {
        startActivity(new Intent(HomeActivity.this, SigninActivity.class));
        onBackPressed();
    }

//    @Override
//    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
//    }
//
//    private void handleSignInResult(GoogleSignInResult result) {
//        if (result.isSuccess()) {
//            GoogleSignInAccount account = result.getSignInAccount();
//            Picasso.get().load(account.getPhotoUrl()).into(signout);
//        } else {
//            gotoMainActivity();
//        }
//    }

    @Override
    protected void onStart() {
        super.onStart();
//        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(googleApiClient);
//        if (opr.isDone()) {
//            GoogleSignInResult result = opr.get();
//            handleSignInResult(result);
//        } else {
//            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
//                @Override
//                public void onResult(@NonNull GoogleSignInResult result) {
//                    handleSignInResult(result);
//                }
//            });
//        }
    }
}