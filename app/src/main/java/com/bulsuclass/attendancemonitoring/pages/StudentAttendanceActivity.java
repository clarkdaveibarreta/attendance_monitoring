package com.bulsuclass.attendancemonitoring.pages;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bulsuclass.attendancemonitoring.R;
import com.bulsuclass.attendancemonitoring.controllers.AttendanceController;
import com.bulsuclass.attendancemonitoring.controllers.StudentController;
import com.bulsuclass.attendancemonitoring.controllers.SubjectController;
import com.bulsuclass.attendancemonitoring.models.Attendance;
import com.bulsuclass.attendancemonitoring.models.Student;
import com.bulsuclass.attendancemonitoring.models.Subject;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class StudentAttendanceActivity extends AppCompatActivity {

    private String subjectId;
    private String attendanceDate;
    private boolean absents = true;

    private Subject subject;
    private Student student;
    private Attendance attendance;

    private File filePath;

    private ArrayList<Student> students;
    private ArrayList<Attendance> attendances;

    private SubjectController subjectController;
    private StudentController studentController;
    private AttendanceController attendanceController;

    private ServerSocket serverSocket;
    private Socket tempClientSocket;
    Thread serverThread = null;
    public static final int SERVER_PORT = 3004;
    private Handler handler;

    WifiManager wifiManager;
    String ip;

    TextView studentAttendanceTitle, studentToggleText, ipTextView;
    Button studentAttendanceToggleButton, startAttendanceButton, generateExcelButton;
    ListView studentAttendanceListView;
    ArrayList<String> studentList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_attendance);

        // Initialize views
        studentAttendanceTitle = findViewById(R.id.student_attendance_title);
        studentToggleText = findViewById(R.id.student_attendance_toggle_text);
        ipTextView = findViewById(R.id.ip_text_view);
        studentAttendanceListView = findViewById(R.id.student_attendance_list);
        studentAttendanceToggleButton = findViewById(R.id.student_attendance_toggle_button);
        startAttendanceButton = findViewById(R.id.student_attendance_open);
        generateExcelButton = findViewById(R.id.generate_excel_button);

        handler = new Handler();

        // Initialize values
        setupInitialValues();
        setupButtons();

        ActivityCompat.requestPermissions(this ,
                new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                PackageManager.PERMISSION_GRANTED);

    }

    private void setupInitialValues() {

        Intent intent = getIntent();
        subjectId = intent.getStringExtra(AttendanceHistoryActivity.ATTENDACE_SUBJECT_ID_EXTRA);
        attendanceDate = intent.getStringExtra(AttendanceHistoryActivity.ATTENDANCE_DATE);

        subjectController = new SubjectController(this);
        studentController = new StudentController(this);
        attendanceController = new AttendanceController(this);

        subject = subjectController.getSubjectById(subjectId);


        studentAttendanceTitle.setText(subject.getName() + " (" + subject.getTime() + ")");
        refreshCurrentList();

        String subjectName = subject.getName();
//        filePath = new File(Environment.getExternalStorageDirectory() + "/" + subjectName + "_" + attendanceDate + ".xls");
    }

    private void setupButtons() {
        studentAttendanceToggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleStudentList();
            }
        });

        startAttendanceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startServer();
            }
        });

        generateExcelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generateExcelFile();
            }
        });

    }

    private void generateExcelFile() {

        ArrayList<Attendance> attendances = attendanceController.getAttendanceBySubjectAndDate(studentController, subject, attendanceDate, "yes");
        ArrayList<Attendance> temps = attendanceController.getAttendanceBySubjectAndDate(studentController, subject, attendanceDate, "no");
        attendances.addAll(temps);

        String subjectName = subject.getName();


        // Process for generating excel file
        HSSFWorkbook hssfWorkbook = new HSSFWorkbook();
        HSSFSheet hssfSheet = hssfWorkbook.createSheet();

        HSSFRow row = hssfSheet.createRow(0);
        HSSFCell cell = row.createCell(0);

        cell.setCellValue("Attendance for: " + subjectName + " ( " + attendanceDate + " )");

        row = hssfSheet.createRow(2);

        cell = row.createCell(0);
        cell.setCellValue("STUDENT ID");

        cell = row.createCell(1);
        cell.setCellValue("STUDENT NAME");

        cell = row.createCell(2);
        cell.setCellValue("ATTENDANCE");

        int index = 3;

        for(Attendance att : attendances) {

            Student student = att.getStudent();
            String studentId = student.getId();
            String studentName = student.getStudentName();
            String attended = att.getAttended();

            if (attended.equals("no"))

                attended="Absent";
            else if(attended.equals("yes"))
                attended="Present";
            else if(attended.equals("late")) {
                attended = "Late";
            }



            row = hssfSheet.createRow(index);

            cell = row.createCell(0);
            cell.setCellValue(studentId);

            cell = row.createCell(1);
            cell.setCellValue(studentName);

            cell = row.createCell(2);
            cell.setCellValue(attended);

            index++;

        }

        try {

            String [] n = attendanceDate.split("/");
            String date = "";
            for(int i = 0; i < n.length; i++) {
                date = date + n[i];
            }

            filePath = new File(Environment.getExternalStorageDirectory() + "/" + subjectName + "-" + date + ".xls");
            if(!filePath.exists())
                filePath.createNewFile();


            FileOutputStream fileOutputStream = new FileOutputStream(filePath);
            hssfWorkbook.write(fileOutputStream);

            if(fileOutputStream != null) {
                fileOutputStream.flush();
                fileOutputStream.close();
            }
            Toast.makeText(this, "Created report successfully", Toast.LENGTH_SHORT).show();

        }
        catch(Exception e) {
            e.printStackTrace();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    private void toggleStudentList() {
        if(absents) {
            studentToggleText.setText("PRESENTS");
            setListView("yes");
            absents = false;
        }
        else {
            studentToggleText.setText("ABSENTS");
            setListView("no");
            absents = true;
        }
    }

    private void refreshCurrentList() {
        if(absents) {
            setListView("no");
        }
        else {
            setListView("yes");
        }
    }

    private void startServer() {

        this.serverThread = new Thread(new ServerThread());
        this.serverThread.start();
    }

    public void showMessage(final String message) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                if(message == "" || message == null) {
                    sendMessage("Student number is required");
                }
                else if(message.contains("%,%")) {

                    String[] decodedMessage = null;
                    String studentId = "";
                    String ipAddress = "";

                    try {
                        decodedMessage = message.split("%,%");
                        studentId = decodedMessage[0];
                        ipAddress = decodedMessage[1];
                    }
                    catch(Exception e) {
                        Toast.makeText(StudentAttendanceActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    if(!attendanceController.deviceHadAttendance(subjectId, ipAddress, attendanceDate)) {
                        if(attendanceController.updateAttendance(subjectId, studentId, ipAddress, attendanceDate)) {
                            refreshCurrentList();
                            sendMessage("You have successfully signed for attendance");
                        }
                        else {
                            sendMessage("Student number is not registered in this class");
                        }
                    }
                    else {
                        sendMessage("You have already signed for attendance today");
                    }
                }
            }
        });
    }

    private void sendMessage(final String message) {
        try {

            if(tempClientSocket != null) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        PrintWriter out = null;
                        try {
                            out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(tempClientSocket.getOutputStream())), true);
                        }
                        catch(Exception e) {
                            e.printStackTrace();
                        }
                        out.println(message);
                    }
                }).start();
            }

        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    private void setListView(String attended) {

        attendances = attendanceController.getAttendanceBySubjectAndDate(studentController, subject, attendanceDate, attended);
        students = new ArrayList<>();
        studentList = new ArrayList<>();

        for(Attendance att : attendances) {
            students.add(att.getStudent());

            studentList.add("(" + att.getStudent().getId() + ") " +
                    att.getStudent().getLastName() + ", " + att.getStudent().getFirstName() + " " +
                    att.getStudent().getMi() + ".");

        }

        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, studentList);
        studentAttendanceListView.setAdapter(adapter);
        studentAttendanceListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                markLate(position);
            }
        });

    }

    private void markLate(int pos) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Late Student")
                .setMessage("Do you want to mark this student as late?")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Attendance attendance = attendances.get(pos);
                        attendanceController.lateStudent(attendance.getAttendanceId());
                    }
                })
                .show();
    }

    class ServerThread implements Runnable {

        @Override
        public void run() {
            Socket socket;
            try {

                if(serverSocket == null) {
                    serverSocket = new ServerSocket(SERVER_PORT);
                }

                wifiManager = (WifiManager) StudentAttendanceActivity.this.getApplicationContext().getSystemService(WIFI_SERVICE);
                ip = Formatter.formatIpAddress(wifiManager.getConnectionInfo().getIpAddress());
                ipTextView.setText("Started server in: " + ip);

            }
            catch(Exception e) {
                e.printStackTrace();
            }

            if(serverSocket != null && !serverSocket.isClosed()) {
                while(!Thread.currentThread().isInterrupted()) {

                    try {

                        socket = serverSocket.accept();
                        CommunicationThread commThread = new CommunicationThread(socket);
                        new Thread(commThread).start();

                    }
                    catch(Exception e) {
                        e.printStackTrace();
                        showMessage(e.getMessage());
                    }
                }
            }

        }
    }

    class CommunicationThread implements Runnable {

        private Socket clientSocket;
        private BufferedReader input;

        public CommunicationThread(Socket clientSocket) {

            this.clientSocket = clientSocket;
            tempClientSocket = clientSocket;

            try {

                this.input = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));

            }
            catch(Exception e) {
                e.printStackTrace();
                showMessage(e.getMessage());
            }
            sendMessage("You have connected to the server!");
        }

        @Override
        public void run() {
            while(!Thread.currentThread().isInterrupted()) {
                try {
                    String read = input.readLine();
                    if(read == null || "Disconnect".contentEquals(read)) {
                        Thread.interrupted();
                        read = "Client Disconnected";
                        showMessage(read);
                        break;
                    }
                    showMessage(read);
                }
                catch(Exception e) {
                    e.printStackTrace();
                    showMessage(e.getMessage());
                }
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(serverThread != null) {
            sendMessage("Disconnect");
            serverThread.interrupt();
            serverThread = null;
            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(serverThread != null) {
            sendMessage("Disconnect");
            serverThread.interrupt();
            serverThread = null;

            try {
                serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}