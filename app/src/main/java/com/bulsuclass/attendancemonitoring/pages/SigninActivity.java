package com.bulsuclass.attendancemonitoring.pages;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bulsuclass.attendancemonitoring.MainActivity;
import com.bulsuclass.attendancemonitoring.R;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;


public class SigninActivity extends AppCompatActivity
//        implements GoogleApiClient.OnConnectionFailedListener
        {
//implements GoogleApiClient.OnConnectionFailedListener {

    private SignInButton signInButton;
    private GoogleApiClient googleApiClient;
    private static final int SIGN_IN = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        SharedPreferences prefs = getSharedPreferences("PREFS", MODE_PRIVATE);
        String token = prefs.getString("token", null);

        if(token == null) {
            startActivity(new Intent(this, LoginActivity.class));
            this.finish();
        }
        else {
            startActivity(new Intent(this, HomeActivity.class));
            this.finish();
        }

//
//          GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
//
//           googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this , this)
//              .addApi(Auth.GOOGLE_SIGN_IN_API , gso).build();
//
//        signInButton = (SignInButton) findViewById(R.id.google_sign_in);
//        signInButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //startActivity(new Intent(MainActivity.this , Account.class));
//                //finish();
//
//                Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
//                startActivityForResult(intent , SIGN_IN);
//            }
//
//        });


    }

//    @Override
//    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
//
//    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if(requestCode == SIGN_IN){
//            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
//            if(result.isSuccess()){
//                startActivity(new Intent(SigninActivity.this , HomeActivity.class));
//                finish();
//            }   else {
//                Toast.makeText(this, "Login Failed", Toast.LENGTH_SHORT).show();
//            }
//        }
//        else {
//            Toast.makeText(this, "Login Failed", Toast.LENGTH_SHORT).show();
//        }
//    }
}