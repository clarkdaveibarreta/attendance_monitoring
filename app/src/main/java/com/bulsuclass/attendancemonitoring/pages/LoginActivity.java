package com.bulsuclass.attendancemonitoring.pages;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bulsuclass.attendancemonitoring.R;
import com.bulsuclass.attendancemonitoring.services.DatabaseHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    EditText email, password;
    Button loginButton, registerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        initVariables();

    }


    private void initVariables() {

        email = findViewById(R.id.login_email);
        password = findViewById(R.id.login_password);

        loginButton = findViewById(R.id.login_log_button);
        registerButton = findViewById(R.id.login_reg_button);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

    }

    private void register() {
        startActivity(new Intent(this, RegisterActivity.class));
        this.finish();
    }

    private void login() {

        DatabaseHelper helper = new DatabaseHelper(this);
        String url = helper.API_URL + "/login";

        String _email = email.getText().toString();
        String _password = password.getText().toString();

        RequestQueue q = Volley.newRequestQueue(this);

        StringRequest request = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {

                            JSONObject res = new JSONObject(response);
                            JSONObject user = res.getJSONObject("user");
                            String id = user.getString("id");
                            String id_number = user.getString("id_number");
                            String name = user.getString("name");
                            String email = user.getString("email");
                            String role = user.getString("role");
                            String token = res.getString("token");

                            SharedPreferences prefs = getSharedPreferences("PREFS", MODE_PRIVATE);
                            SharedPreferences.Editor editor = prefs.edit();

                            editor.putString("user.id", id);
                            editor.putString("user.id_number", id_number);
                            editor.putString("user.name", name);
                            editor.putString("user.email", email);
                            editor.putString("user.role", role);
                            editor.putString("token", token);

                            editor.apply();

                            login_r(true);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        login_r(false);
                    }
                }){
            @Nullable
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", _email);
                params.put("password", _password);

                return params;
            }
        };

        q.add(request);

    }

    private void login_r(boolean response) {

        if(response) {
            startActivity(new Intent(this, HomeActivity.class));
            this.finish();
        }
        else {
            Toast.makeText(this, "Login Failed", Toast.LENGTH_SHORT).show();
        }

    }

}