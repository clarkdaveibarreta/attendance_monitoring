package com.bulsuclass.attendancemonitoring.services;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String API_URL = "https://attmon.herokuapp.com/api";
    private static final String DATABASE_NAME = "attendance_monitoring.db";
    private static final int DATABASE_VERSION = 1;

    // subject table
    public static final String SUBJECT_TABLE_NAME = "subjects";
    public static final String SUBJECT_ID = "id";
    public static final String SUBJECT_NAME = "name";
    public static final String SUBJECT_TIME = "time";
    private static final String create_subject_table_query = "CREATE TABLE IF NOT EXISTS " + SUBJECT_TABLE_NAME + " " +
            "(" + SUBJECT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            ""  + SUBJECT_NAME + " TEXT," +
            ""  + SUBJECT_TIME + " TEXT" + ")";

    // students table
    public static final String STUDENT_TABLE_NAME = "students";
    public static final String STUDENT_ID = "id";
    public static final String STUDENT_FIRST_NAME = "first_name";
    public static final String STUDENT_LAST_NAME = "last_name";
    public static final String STUDENT_M_I = "m_i";
    private static final String create_student_table_query = "CREATE TABLE IF NOT EXISTS " + STUDENT_TABLE_NAME + " " +
            "(" + STUDENT_ID + " TEXT PRIMARY KEY," +
            ""  + STUDENT_FIRST_NAME + " TEXT," +
            ""  + STUDENT_LAST_NAME + " TEXT," +
            ""  + STUDENT_M_I + " TEXT" + ")";

    // classes table
    public static final String CLASSES_TABLE_NAME = "classes";
    public static final String CLASSES_ID = "id";
    public static final String CLASSES_SUBJECT_ID = "subject_id";
    public static final String CLASSES_STUDENT_ID = "student_id";
    private static final String create_classes_table_query = "CREATE TABLE IF NOT EXISTS " + CLASSES_TABLE_NAME + " " +
            "(" +   CLASSES_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            ""  +   CLASSES_SUBJECT_ID + " INTEGER, " +
            ""  +   CLASSES_STUDENT_ID + " INTEGER )";

    // attendance table
    public static final String ATTENDANCE_TABLE_NAME = "attendances";
    public static final String ATTENDANCE_ID = "id";
    public static final String ATTENDANCE_SUBJECT_ID = "subject_id";
    public static final String ATTENDANCE_STUDENT_ID = "student_id";
    public static final String ATTENDANCE_DATE = "date";
    public static final String ATTENDANCE_ATTENDED = "attended";
    public static final String ATTENDANCE_IP_ADDRESS = "mac_address";
    private static final String create_attendances_table_query = "CREATE TABLE IF NOT EXISTS " + ATTENDANCE_TABLE_NAME + " " +
            "(" +   ATTENDANCE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            ""  +   ATTENDANCE_SUBJECT_ID + " INTEGER, " +
            ""  +   ATTENDANCE_STUDENT_ID + " INTEGER, " +
            ""  +   ATTENDANCE_DATE + " TEXT, " +
            ""  +   ATTENDANCE_ATTENDED + " TEXT, " +
            ""  +   ATTENDANCE_IP_ADDRESS + " TEXT )";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // Create subject table
        db.execSQL(create_subject_table_query);

        // Create student table
        db.execSQL(create_student_table_query);

        // Create classes table
        db.execSQL(create_classes_table_query);

        // Create attendances table
        db.execSQL(create_attendances_table_query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + SUBJECT_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + STUDENT_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + CLASSES_TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ATTENDANCE_TABLE_NAME);

        onCreate(db);
    }

    public SQLiteDatabase getWritables() {
        return this.getWritableDatabase();
    }

    public SQLiteDatabase getReadable() {
        return this.getReadableDatabase();
    }
}
